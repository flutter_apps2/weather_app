import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:weather_app/api/MapApi.dart';
import 'package:weather_app/ui/Weather.dart';

main(){
  
  test('MapApi getWeather Method', () async{

      final double lat = 35.57;
      final double lon = 136.77;
      
      final mapJson = {
        "main":{"temp":18},
        "weather":[{"main":"clouds"}],
      };

      final mapApi = MapApi.getInstance();

      mapApi.client = MockClient((request) async {
        return Response(json.encode(mapJson),200);
      });

      final weatherItem = await mapApi.getWeather(lat: lat, lon: lon);

      expect(weatherItem.temp, equals(18));

  });
}