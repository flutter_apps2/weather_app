import 'package:flutter/material.dart';
import 'package:weather_app/api/LocationApi.dart';
import 'package:weather_app/api/MapApi.dart';
import 'package:weather_app/model/WeatherData.dart';
import 'package:weather_app/ui/Weather.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  WeatherData _weatherData;

  @override
  void initState() {
    super.initState();
    getCurrentLocation();
  }
  
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.blue,
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      body: _weatherData != null ? Weather(weatherData: _weatherData) : 
        Center(
          child: CircularProgressIndicator(
            strokeWidth: 4.0,
            valueColor: AlwaysStoppedAnimation(Colors.white),
          ),
          )
    );
  }

  getCurrentLocation() async{
    LocationApi locationApi = LocationApi.getInstance();
    final location = await locationApi.getLocation();
    loadWeather(lat: location.lat, lon: location.lon);
  }

  loadWeather({double lat, double lon}) async{
    MapApi mapApi = MapApi.getInstance();
    final data = await mapApi.getWeather(lat: lat, lon: lon);
    setState(() {
      this._weatherData = data;
    });
  }
}
